
let express = require("express");

const PORT = 4000;

let users = [];
let app = express();
 
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res)=> res.send(`Welcome to the home page.`));

  
app.post("/signup", (req, res) => {
 
 if (req.body.username !== "" && req.body.password !== "" ) {

      res.send(`User, ${req.body.username} successfully registered`);
      
      
          users.push({
              username: req.body.username,
              password: req.body.password
          });

      console.log(users);
  }
  else {
    res.send(`Please input BOTH username and password`);
  }

});


app.get("/users", (req, res)=> res.send(JSON.stringify(users)));
console.log(users);
app.listen(PORT, ()=> {
  console.log(`Server running at port ${PORT}`)
});